from ..base.test import Test, grade
import copy
import socket, struct
import time

class CRC_Checking(Test):
    description = "CRC Checking"
    order = 2
    enabled = True
    test_order = ['test_initial_crc_checking']
    save_judge_mode=False

    def before(self):
        client_count = 5
        self.kill_clients()
        self.new_map()
        client_dict = {0:'c', 1:'c', 2:'c', 3:'c', 10:'r'}
        self.start_clients(client_dict=client_dict)


        for client in self.clients.itervalues():
            client.wait_for_start()
        time.sleep(self.sleep_time)

    def after(self):
        self.kill_clients()
        self.free_map()

    def save_judge(self, path):
        self.client_manager.save_judge_all(path)

    def load_judge(self, path):
        self.client_manager.load_judge_all(path)
    # ===================================================================================================================================================== #

    @grade(100)
    def test_initial_crc_checking(self):
        self.clients[1].write_io('check all of 1')
        self.clients[2].write_io('check 2 of 1')
        self.clients[2].write_io('check 3 of 1')
        self.clients[2].write_io('check 4 of 1')
        self.clients[2].write_io('check 5 of 1')
        self.clients[2].write_io('check 6 of 1')
        self.clients[3].write_io('check all of 1')
        self.clients[3].write_io('check all of 2')
        time.sleep(self.sleep_time)

        self.client_manager.get_clients_ready_to_judge('log/test_initial_crc_checking')
        if not self.save_judge_mode:
            self.assert_true(self.check_output(1), message='Output for node 1 did not match', end=False, grade=29)
            self.assert_true(self.check_output(2), message='Output for node 2 did not match', end=False, grade=39)
            self.assert_true(self.check_output(3), message='Output for node 3 did not match', end=False, grade=32)
# ===================================================================================================================================================== #
