from ..base.test import Test, grade
import copy
import socket, struct
import time

class Connection_Status(Test):
    description = "Connection Status"
    order = 4
    enabled = True
    test_order = ['test_choking_status_01','test_choking_status_02',
        'test_intersting_status_01','test_intersting_status_02',
        'test_failed_request_choked','test_failed_request_uninterested']
    save_judge_mode=False

    def before(self):
        client_count = 5
        self.kill_clients()
        self.new_map()
        client_dict = {0:'c', 1:'c', 2:'c', 3:'c', 10:'r'}
        self.start_clients(client_dict=client_dict)

        for client in self.clients.itervalues():
            client.wait_for_start()
        time.sleep(self.sleep_time)

    def after(self):
        self.kill_clients()
        self.free_map()

    def save_judge(self, path):
        self.client_manager.save_judge_all(path)

    def load_judge(self, path):
        self.client_manager.load_judge_all(path)
    # ===================================================================================================================================================== #
    @grade(16)
    def test_choking_status_01(self):
        self.clients[2].write_io('get peers of 2')
        time.sleep(self.sleep_time)
        self.clients[3].write_io('get peers of 1')
        time.sleep(self.sleep_time)
        self.clients[3].write_io('connect to deluge from 1')
        time.sleep(self.sleep_time)
        self.clients[3].write_io('choke to deluge of 1')
        time.sleep(self.sleep_time)

        self.client_manager.get_clients_ready_to_judge('log/test_choking_status_01')
        if not self.save_judge_mode:
            self.assert_true(self.check_output(0), message='Output for node 0 did not match', end=False, grade=2)
            self.assert_true(self.check_output(2), message='Output for node 2 did not match', end=False, grade=2)
            self.assert_true(self.check_output(3), message='Output for node 3 did not match', end=False, grade=4)

            self.assert_true(self.check_send_frames(0), message='sent frames node 0 did not match', end=False, grade=2)
            self.assert_true(self.check_send_frames(2), message='sent frames node 2 did not match', end=False, grade=4)
            self.assert_true(self.check_send_frames(3), message='sent frames node 3 did not match', end=False, grade=2)

    @grade(16)
    def test_choking_status_02(self):
        self.clients[2].write_io('get peers of 2')
        time.sleep(self.sleep_time)
        self.clients[3].write_io('get peers of 1')
        time.sleep(self.sleep_time)
        self.clients[3].write_io('connect to deluge from 1')
        time.sleep(self.sleep_time)
        self.clients[3].write_io('unchoke to deluge of 1')
        time.sleep(self.sleep_time)

        self.client_manager.get_clients_ready_to_judge('log/test_choking_status_02')
        if not self.save_judge_mode:
            self.assert_true(self.check_output(0), message='Output for node 0 did not match', end=False, grade=2)
            self.assert_true(self.check_output(2), message='Output for node 2 did not match', end=False, grade=2)
            self.assert_true(self.check_output(3), message='Output for node 3 did not match', end=False, grade=4)

            self.assert_true(self.check_send_frames(0), message='sent frames node 0 did not match', end=False, grade=2)
            self.assert_true(self.check_send_frames(2), message='sent frames node 2 did not match', end=False, grade=4)
            self.assert_true(self.check_send_frames(3), message='sent frames node 3 did not match', end=False, grade=2)

    @grade(16)
    def test_intersting_status_01(self):
        self.clients[2].write_io('get peers of 2')
        time.sleep(self.sleep_time)
        self.clients[3].write_io('get peers of 1')
        time.sleep(self.sleep_time)
        self.clients[3].write_io('connect to deluge from 1')
        time.sleep(self.sleep_time)
        self.clients[3].write_io('get uninterested to deluge of 1')
        time.sleep(self.sleep_time)

        self.client_manager.get_clients_ready_to_judge('log/test_intersting_status_01')
        if not self.save_judge_mode:
            self.assert_true(self.check_output(0), message='Output for node 0 did not match', end=False, grade=2)
            self.assert_true(self.check_output(2), message='Output for node 2 did not match', end=False, grade=2)
            self.assert_true(self.check_output(3), message='Output for node 3 did not match', end=False, grade=4)

            self.assert_true(self.check_send_frames(0), message='sent frames node 0 did not match', end=False, grade=2)
            self.assert_true(self.check_send_frames(2), message='sent frames node 2 did not match', end=False, grade=4)
            self.assert_true(self.check_send_frames(3), message='sent frames node 3 did not match', end=False, grade=2)

    @grade(16)
    def test_intersting_status_02(self):
        self.clients[2].write_io('get peers of 2')
        time.sleep(self.sleep_time)
        self.clients[3].write_io('get peers of 1')
        time.sleep(self.sleep_time)
        self.clients[3].write_io('connect to deluge from 1')
        time.sleep(self.sleep_time)
        self.clients[3].write_io('get interested to deluge of 1')
        time.sleep(self.sleep_time)

        self.client_manager.get_clients_ready_to_judge('log/test_intersting_status_02')
        if not self.save_judge_mode:
            self.assert_true(self.check_output(0), message='Output for node 0 did not match', end=False, grade=2)
            self.assert_true(self.check_output(2), message='Output for node 2 did not match', end=False, grade=2)
            self.assert_true(self.check_output(3), message='Output for node 3 did not match', end=False, grade=4)

            self.assert_true(self.check_send_frames(0), message='sent frames node 0 did not match', end=False, grade=2)
            self.assert_true(self.check_send_frames(2), message='sent frames node 2 did not match', end=False, grade=4)
            self.assert_true(self.check_send_frames(3), message='sent frames node 3 did not match', end=False, grade=2)

    @grade(18)
    def test_failed_request_uninterested(self):
        self.clients[2].write_io('get peers of 2')
        time.sleep(self.sleep_time)
        self.clients[3].write_io('get peers of 1')
        time.sleep(self.sleep_time)
        self.clients[3].write_io('connect to deluge from 1')
        time.sleep(self.sleep_time)
        self.clients[3].write_io('download piece 0 from deluge of 1')
        time.sleep(self.sleep_time)

        self.client_manager.get_clients_ready_to_judge('log/test_failed_request_uninterested')
        if not self.save_judge_mode:
            self.assert_true(self.check_output(0), message='Output for node 0 did not match', end=False, grade=1)
            self.assert_true(self.check_output(2), message='Output for node 2 did not match', end=False, grade=1)
            self.assert_true(self.check_output(3), message='Output for node 3 did not match', end=False, grade=7)

            self.assert_true(self.check_send_frames(0), message='sent frames node 0 did not match', end=False, grade=1)
            self.assert_true(self.check_send_frames(2), message='sent frames node 2 did not match', end=False, grade=7)
            self.assert_true(self.check_send_frames(3), message='sent frames node 3 did not match', end=False, grade=1)

    @grade(18)
    def test_failed_request_choked(self):
        self.clients[2].write_io('get peers of 2')
        time.sleep(self.sleep_time)
        self.clients[3].write_io('get peers of 1')
        time.sleep(self.sleep_time)
        self.clients[3].write_io('connect to deluge from 1')
        time.sleep(self.sleep_time)
        self.clients[3].write_io('get interested to deluge of 1')
        time.sleep(self.sleep_time)
        self.clients[3].write_io('download piece 0 from deluge of 1')
        time.sleep(self.sleep_time)

        self.client_manager.get_clients_ready_to_judge('log/test_failed_request_choked')
        if not self.save_judge_mode:
            self.assert_true(self.check_output(0), message='Output for node 0 did not match', end=False, grade=1)
            self.assert_true(self.check_output(2), message='Output for node 2 did not match', end=False, grade=7)
            self.assert_true(self.check_output(3), message='Output for node 3 did not match', end=False, grade=1)

            self.assert_true(self.check_send_frames(0), message='sent frames node 0 did not match', end=False, grade=1)
            self.assert_true(self.check_send_frames(2), message='sent frames node 2 did not match', end=False, grade=7)
            self.assert_true(self.check_send_frames(3), message='sent frames node 3 did not match', end=False, grade=1)



# ===================================================================================================================================================== #
