from ..base.test import Test, grade
import copy
import socket, struct
import time

class Connection_Stablishing(Test):
    description = "Connection Stablishing"
    order = 3
    enabled = True
    test_order = ['test_send_handshake___peer_list_updating']
    save_judge_mode=False

    def before(self):
        client_count = 5
        self.kill_clients()
        self.new_map()
        client_dict = {0:'c', 1:'c', 2:'c', 3:'c', 10:'r'}
        self.start_clients(client_dict=client_dict)

        for client in self.clients.itervalues():
            client.wait_for_start()
        time.sleep(self.sleep_time)

    def after(self):
        self.kill_clients()
        self.free_map()

    def save_judge(self, path):
        self.client_manager.save_judge_all(path)

    def load_judge(self, path):
        self.client_manager.load_judge_all(path)
    # ===================================================================================================================================================== #

    @grade(100)
    def test_send_handshake___peer_list_updating(self):
        self.clients[2].write_io('get peers of 1')
        time.sleep(self.sleep_time)
        self.clients[1].write_io('get peers of 1')
        time.sleep(self.sleep_time)
        self.clients[1].write_io('connect to deluge from 1')
        time.sleep(self.sleep_time)

        self.client_manager.get_clients_ready_to_judge('log/test_send_handshake___peer_list_updating')
        if not self.save_judge_mode:
            self.assert_true(self.check_output(0), message='Output for node 0 did not match', end=False, grade=5)
            self.assert_true(self.check_output(1), message='Output for node 1 did not match', end=False, grade=18)
            self.assert_true(self.check_output(2), message='Output for node 2 did not match', end=False, grade=23)

            self.assert_true(self.check_send_frames(0), message='sent frames node 0 did not match', end=False, grade=4)
            self.assert_true(self.check_send_frames(1), message='sent frames node 1 did not match', end=False, grade=25)
            self.assert_true(self.check_send_frames(2), message='sent frames node 2 did not match', end=False, grade=25)

# ===================================================================================================================================================== #
