from ..base.test import Test, grade
import copy
import socket, struct
import time

class Data_Transfering(Test):
    description = "Transfering Data"
    order = 5
    enabled = True
    test_order = ['test_piece_request_before_cheking','test_piece_request_after_checking',
        'test_pieces_transfer_and_crc_checking']
    save_judge_mode=False

    def before(self):
        client_count = 5
        self.kill_clients()
        self.new_map()
        client_dict = {0:'c', 1:'c', 2:'c', 3:'c', 10:'r'}
        self.start_clients(client_dict=client_dict)


        for client in self.clients.itervalues():
            client.wait_for_start()
        time.sleep(self.sleep_time)

    def after(self):
        self.kill_clients()
        self.free_map()

    def save_judge(self, path):
        self.client_manager.save_judge_all(path)

    def load_judge(self, path):
        self.client_manager.load_judge_all(path)
    # ===================================================================================================================================================== #
    @grade(25)
    def test_piece_request_before_cheking(self):
        self.clients[2].write_io('get peers of 1')
        time.sleep(self.sleep_time)
        self.clients[1].write_io('get peers of 1')
        time.sleep(self.sleep_time)
        self.clients[1].write_io('connect to deluge from 1')
        time.sleep(self.sleep_time)
        self.clients[2].write_io('get interested to utorrent of 1')
        time.sleep(self.sleep_time)
        self.clients[1].write_io('unchoke to deluge of 1')
        time.sleep(self.sleep_time)
        self.clients[2].write_io('download piece 1 from utorrent of 1')
        time.sleep(self.sleep_time)

        self.client_manager.get_clients_ready_to_judge('log/test_piece_request_before_cheking')
        if not self.save_judge_mode:
            self.assert_true(self.check_output(0), message='Output for node 0 did not match', end=False, grade=1)
            self.assert_true(self.check_output(1), message='Output for node 1 did not match', end=False, grade=8)
            self.assert_true(self.check_output(2), message='Output for node 2 did not match', end=False, grade=2)

            self.assert_true(self.check_send_frames(0), message='sent frames node 0 did not match', end=False, grade=1)
            self.assert_true(self.check_send_frames(1), message='sent frames node 1 did not match', end=False, grade=3)
            self.assert_true(self.check_send_frames(2), message='sent frames node 2 did not match', end=False, grade=10)

    @grade(45)
    def test_piece_request_after_checking(self):
        self.clients[2].write_io('get peers of 1')
        time.sleep(self.sleep_time)
        self.clients[1].write_io('get peers of 1')
        self.clients[1].write_io('check all of 1')
        time.sleep(self.sleep_time)
        self.clients[1].write_io('connect to deluge from 1')
        time.sleep(self.sleep_time)
        self.clients[2].write_io('get interested to utorrent of 1')
        time.sleep(self.sleep_time)
        self.clients[1].write_io('unchoke to deluge of 1')
        time.sleep(self.sleep_time)
        self.clients[2].write_io('download piece 0 from utorrent of 1')
        time.sleep(self.sleep_time)

        self.client_manager.get_clients_ready_to_judge('log/test_piece_request_after_checking')
        if not self.save_judge_mode:
            self.assert_true(self.check_output(0), message='Output for node 0 did not match', end=False, grade=1)
            self.assert_true(self.check_output(1), message='Output for node 1 did not match', end=False, grade=15)
            self.assert_true(self.check_output(2), message='Output for node 2 did not match', end=False, grade=2)

            self.assert_true(self.check_send_frames(0), message='sent frames node 0 did not match', end=False, grade=1)
            self.assert_true(self.check_send_frames(1), message='sent frames node 1 did not match', end=False, grade=12)
            self.assert_true(self.check_send_frames(2), message='sent frames node 2 did not match', end=False, grade=14)

    @grade(30)
    def test_pieces_transfer_and_crc_checking(self):
        self.clients[2].write_io('get peers of 1')
        time.sleep(self.sleep_time)
        self.clients[1].write_io('get peers of 1')
        self.clients[1].write_io('check all of 1')
        time.sleep(self.sleep_time)
        self.clients[1].write_io('connect to deluge from 1')
        time.sleep(self.sleep_time)
        self.clients[2].write_io('get interested to utorrent of 1')
        time.sleep(self.sleep_time)
        self.clients[1].write_io('unchoke to deluge of 1')
        time.sleep(self.sleep_time)
        self.clients[2].write_io('download piece 0 from utorrent of 1')
        time.sleep(self.sleep_time)
        self.clients[2].write_io('download piece 1 from utorrent of 1')
        time.sleep(self.sleep_time)
        self.clients[2].write_io('check all of 1')
        time.sleep(self.sleep_time)

        self.client_manager.get_clients_ready_to_judge('log/test_pieces_transfer_and_crc_checking')
        if not self.save_judge_mode:
            self.assert_true(self.check_output(0), message='Output for node 0 did not match', end=False, grade=1)
            self.assert_true(self.check_output(1), message='Output for node 1 did not match', end=False, grade=3)
            self.assert_true(self.check_output(2), message='Output for node 2 did not match', end=False, grade=8)

            self.assert_true(self.check_send_frames(0), message='sent frames node 0 did not match', end=False, grade=1)
            self.assert_true(self.check_send_frames(1), message='sent frames node 1 did not match', end=False, grade=9)
            self.assert_true(self.check_send_frames(2), message='sent frames node 2 did not match', end=False, grade=8)

# ===================================================================================================================================================== #
