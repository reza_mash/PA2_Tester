import ir.sharif.ce.partov.base.ClientFramework;
import ir.sharif.ce.partov.utils.Utility;

public class Main {
	private static boolean runCF(ClientFramework cf){
		return cf.connectToServer() && 	cf.doInitialNegotiations() && cf.startSimulation();
	}
	public static void main(String[] args) {
//		args = "--ip 213.233.169.80 --port 7890 --map single_tracker_3_peers --node node10 --user behroozikhah --pass 1234567 --id behroozikhah".split(" ");
		ClientFramework cf = new ClientFramework(args);
		runCF(cf);
		ClientFramework.destroy();
	}
	
	static void printBytes(byte[] bytes){
		for (int i=0 ; i<bytes.length ; i++){
			System.out.println(Utility.byteToHex(bytes[i]));
			bytes[i]++;
		}
	}
}
