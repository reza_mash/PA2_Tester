package ir.sharif.ce.partov.user;

import java.util.HashMap;
//import java.util.Scanner;

import ir.sharif.ce.partov.base.ClientFramework;
import ir.sharif.ce.partov.base.Frame;
import ir.sharif.ce.partov.base.Machine;
import ir.sharif.ce.partov.utils.Utility;

public class SimulateMachine extends Machine {
	public SimulateMachine(ClientFramework clientFramework, int count) {
		super(clientFramework, count);
		// The machine instantiated.
		// Interfaces are not valid at this point.
	}
	private HashMap<Integer, Integer> IPtoIface = new HashMap<>();
	private HashMap<Integer, byte[]> IPtoMac = new HashMap<>();

	public void initialize() {
		// TODO: Initialize your program here; interfaces are valid now.
		String[] info = this.getCustomInformation().split("\n");
		for(String line: info){
			String[] splited = line.split("    ");
			int IP = Utility.getIP(splited[0]);
			int iface = Utility.Dec(splited[1]);
			byte[] Mac = Utility.getMac(splited[2]);
			IPtoIface.put(IP, iface);
			IPtoMac.put(IP, Mac);
		}
	}

	/**
	 * This method is called from the main thread. Also ownership of the data of
	 * the frame is not with you. If you need it, make a copy for yourself.
	 */
	public void processFrame(Frame frame, int ifaceIndex) {
		
		// TODO: process the raw frame; frame.data points to the frame byte
		System.out.println("Frame received at iface " + ifaceIndex
		+ " with length " + frame.length );
		EthernetHeader eth = new EthernetHeader(frame.data, 0);
		System.out.println("type of ethernet packet is 0x"+ Utility.byteToHex(eth.getTypeinBytes()[0])+Utility.byteToHex(eth.getTypeinBytes()[1]));
		if(eth.getTypeinInt()==((int)IPv4Header.IP_PROTOCOL)){
			if(frame.data.length < 20+14){
				System.err.println("The Packet's length is too low to analyse for IPv4Header");
				return;
			}
			IPv4Header iph = new IPv4Header(frame.data, 14,5);
			if(!IPtoMac.containsKey(iph.getDest())){
				System.err.println("The Dest IP is " + Utility.getIPString(iph.getDest())+ " which is not found in routing table.");
				return;
			}
			eth.setDest(IPtoMac.get(iph.getDest()));
			eth.setSrc(iface[IPtoIface.get(iph.getDest())].mac);
			byte[] data = new byte[frame.data.length];
			System.arraycopy(eth.getData(), 0, data, 0, 14);
			System.arraycopy(frame.data, 14, data, 14, data.length-14);
			System.out.println("A packet with source IP " + Utility.getIPString(iph.getSrc())+" and destination IP " + Utility.getIPString(iph.getDest())+" from interface " + ifaceIndex +" forwarded to interface " + IPtoIface.get(iph.getDest()));
			this.sendFrame(new Frame(data), IPtoIface.get(iph.getDest()));
		}else{
			System.err.println("Only IP packets are supported, The type is ethernetheader is something else.");
		}
	}

	/**
	 * This method will be run from an independent thread. Use it if needed or
	 * simply return. Returning from this method will not finish the execution
	 * of the program.
	 */
	public void run() {
		// TODO: write you business logic here...
//		
//		Scanner s = new Scanner(System.in);
//		System.out.println("Type \"send\" to send a sample frame.");
//		while(!s.nextLine().equals("send"));
//		
//		EthernetHeader eth = new EthernetHeader(EthernetHeader.broadcast, iface[0].mac, IPv4Header.IP_PROTOCOL);
//		IPv4Header iph = new IPv4Header();
//		iph.setTotalLength(100);
//		iph.setDest(IPv4Header.BROADCAST_IP);
//		iph.setSrc(IPv4Header.BROADCAST_IP);
//		iph.setTTL(64);
////		iph.setProtocol(protocol);
////		iph.setChecksum(HeaderChecksum);
////		...
//		
//		byte[] data = new byte[14+100];
//		System.arraycopy(eth.getData(), 0, data, 0, 14);
//		System.arraycopy(iph.getData(), 0, data, 14, iph.getData().length);
//		this.sendFrame(new Frame(114,data), 0);
//		System.out.println("Sample frame sent.");
//		s.close();
	}

}